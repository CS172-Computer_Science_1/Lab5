#include <iostream>
#include <ctime>
#include <cstdlib>
#include <random>
#include <string>
using namespace std;

namespace nsEx24{
	void f_menu(int&, string&); //menu
	void f_i2s(int, string&);	//convert number to string	
	void f_AI(int&, string&);	//randomize
	void f_win(string, string, string&); //decide winner

	int intEx24(){
		char again = 'y';
		do{ //keep running while user plays
			cout << "\nn@xxxx[{::::::::::::::::>  [{(Chose your weapon)}]  <::::::::::::::::}]xxxx@n\n\n";

			//menu block
			int menunumber; string menustring;
			f_menu(menunumber, menustring); //garbageint, Player Weapon String

			//AI block
			int AInumber; string AIstring;
			f_AI(AInumber, AIstring);//garbageint, AI Weapon String

			//winner block
			string winP = menustring, winAI = AIstring, winWIN;
			f_win(winP, winAI, winWIN);

			if (winP != winAI){
				cout << "\tPlayer chose >>" << winP << "<< and AI chose >>" << winAI << "<<"
					<< ". \n\tAnd the winner is.....press 'Enter' to find out (^.^)\n";
				cin.get();
				cin.ignore();
				cout << "\t\t" << winWIN << "\n\n";
			}
			else{ //rerun if duplicates
				cout << "\tPlayer chose >>" << winP << "<< and AI chose >>" << winAI<<"<<";
				cout << "\n\t\tOh man/woman nobody wins, darn it!!! \n"
					<< "\t\t\t/!\\Redraw!/!\\\n";
				cin.get();
				cin.ignore();
				continue;
			}
			cout << "\n\t\tDo you want to play again (y|Y)?  ";
			cin >> again;
		} while (again == 'y' || again == 'Y');
		return 0;
	}

	void f_AI(int& f_AI_int, string& f_AI_str){ //also outputs converted AI Weapon String
		//RANDOMIZER BLOCK
		random_device rd;     // seed engine
		mt19937 rng(rd());    // Mersenne-Twister random engine
		uniform_int_distribution<int> uni(1, 3); // guaranteed unbiased
		f_AI_int = uni(rng); int AIint = f_AI_int; //AI variables for conversion

		if (f_AI_int == 1 || f_AI_int == 2 || f_AI_int == 3)
			f_i2s(f_AI_int, f_AI_str);  //output random number, get AI weapon string
	}

	void f_menu(int& f_menu_int, string& f_menu_str){
		cout << "(1) Rock\n" << "(2) Paper\n" << "(3) Scissors\n";
		cin >> f_menu_int; //player choice
		f_i2s(f_menu_int, f_menu_str); // outputs player choice; grabs Player Weapon String
	}

	void f_i2s(int f_i2s_int, string& f_i2s_str){  //Convert number to string
		if (f_i2s_int == 1) f_i2s_str = "Rock";
		else if (f_i2s_int == 2) f_i2s_str = "Paper";
		else if (f_i2s_int == 3) f_i2s_str = "Scissors";
	}

	void f_win(string f_win_strp, string f_win_strAI, string& f_win_winner){  //Convert number to string
		string p = f_win_strp, ai = f_win_strAI;

		if (p == "Rock" && ai == "Scissors")
			f_win_winner = "The rock smashes the scissors. \n\t\tPlayer won, congrats! ";
		else if (p == "Scissors" && ai == "Paper")
			f_win_winner = "Scissors cuts paper. \n\t\tPlayer won, congrats! ";
		else if (p == "Paper" && ai == "Rock")
			f_win_winner = "Paper wraps rock. \n\t\tPlayer won, congrats! ";
		else if (ai == "Rock" && p == "Scissors")
			f_win_winner = "The rock smashes the scissors. \n\t\tMr Computer won, BOO! ";
		else if (ai== "Scissors" && p == "Paper")
			f_win_winner = "Scissors cuts paper. \n\t\tMr Computer won, BOO! ";
		else if (ai == "Paper" && p== "Rock")
			f_win_winner = "Paper wraps rock. \n\t\tMr Computer won, BOO! ";
	}
}