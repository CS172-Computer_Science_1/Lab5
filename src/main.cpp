#include <iostream>
#include <string>
#include "lab5_ex22_23.h"  
#include "lab5_ex24.h"

using namespace std;

int main(){
	for (int iFor = 0; iFor < 10; iFor++){ //<-Prevent Infinite Loop
		char chChoice = '-';

		cout << "\t\t.:[Please enter exercise number (1, 2, or 3)]:.\n\n";
		cout << "(1) Lab 5, Exercise 22 & 23.\n"
			<< "(2) Lab 5, Exercise 24.\n";

		cin >> chChoice;
		cout << "You selected: " << chChoice << endl;

		switch (chChoice){
		case '1':
			cout << "\n\t\t####Lab 5, Exercise 22 & 23####\n";
			nsEx22_23::intEx22();
			nsEx22_23::intEx23();
			break;
		case '2':
			cout << "\n\t\t\t####Lab 5, Exercise 24####\n";
			nsEx24::intEx24();
			break;
		default:
			cout << "\t\tTry again!!! \n\n";
			break;
			cin.ignore();  //Prevent Letter Loop
		}
	}
}